Changelog
=========

All notable changes to this project will be documented in this file.
This project adheres to `Semantic Versioning <http://semver.org/>`_.

[1.0.1]
*******

Fixed
-----

* Parsing

[1.0.0]
*******

Added
-----

* Method to request daily KNMI data
* Method to request hourly KNMI data
* Tests for the library
* License files for used Apache license
* User documentation
