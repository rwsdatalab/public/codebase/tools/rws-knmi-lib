# -*- coding: utf-8 -*-
"""Documentation about RWS KNMI Lib"""
import logging

logging.getLogger(__name__).addHandler(logging.NullHandler())

__author__ = "RWS Datalab"
__email__ = "datalab.codebase@rws.nl"
__version__ = "1.0.1"
